import React from "react";
import axios from "axios";

export default function Query() {

    const [query, setQuery] = React.useState('')
    const [results, setResults] = React.useState([]);
    // const [results, setResults] = React.useState('');
    // const handleSubmit = async (e) => {
    //     e.preventDefault();
    //     try {
    //         const format = query.charAt(0).toLowerCase() + query.slice(1);
    //         const response = await axios.post('http://localhost:5000/query', { query: format }, {headers: {'Content-Type': 'application/json'}});
    //         console.log(response.data);
    //         setResults(response.data);
    //     } catch(error){
    //         console.error(error);
    //     }
    // };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const format = query.charAt(0).toLowerCase() + query.slice(1);
        const response = await axios.post('http://localhost:5000/query', { query: format }, {headers: {'Content-Type': 'application/json'}});
        console.log(response.data);
        setResults(response.data);
    };

    return (

        // <div className="cntr">
        //     <div className="forms">
        //     <form onSubmit={handleSubmit}>
        //         <input 
        //            type="text"
        //             value={query}
        //             onChange={(e) => setQuery(e.target.value)}
        //             className="input"
        //         />
        //         <button type="submit" className="submitbtn">Submit</button>
        //     </form>
        //     </div>
        //     {results && (
        //         <div className="reswrap">
        //             <h3>Output:</h3>
        //             <p className="resp">{results}</p>
        //         </div>
        //     )}
        // </div>
        <div className="container">


             <form onSubmit={handleSubmit}>
             <input 
                    type="text"
                    value={query}
                    onChange={(e) => setQuery(e.target.value)}
                    className="input"
                />
                <button type="submit" className="submitbtn">Submit</button>
            </form>

            <ul>
                {results.map((result, index) => (
                    <li key={index}>
                        <p>Id:{result.id}</p>
                        <p>Name:{result.name}</p>
                        <p>City:{result.city}</p>
                        <p>Salary:{result.salary}</p>
                        <p>Age:{result.age}</p>
                        <p>Work Experience:{result.workexp}</p>
                    </li>
        ))}
            </ul>
        </div>
    )
}