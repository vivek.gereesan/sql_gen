import openai

openai.api_key = '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'

def generate_query(query):
    
    response = openai.Completion.create(
        engine = 'text-davinci-003',
        prompt = query,
        max_tokens = 60,
        temperature = 0.3,
        top_p = 0.1,
        frequency_penalty = 0.0,
        presence_penalty = 0.0
    )
    
    sql_query = response.choices[0].text.strip()
    
    return sql_query