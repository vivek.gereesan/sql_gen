from flask import Flask, request, jsonify
from database import execute_sql_query
from aicon import generate_query
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route('/query', methods = ['POST'])

def handle_query():
    print("Hello")
    query = request.json['query']
    #query = "Find the users with the name Venkat"
    #query = "select * from users"
    sql_query = generate_query(query)
    print(sql_query)
    res = execute_sql_query(sql_query)
    print(res)
    print(jsonify(res))
    return jsonify(res)

if __name__ == '__main__':
    app.run(debug=True)