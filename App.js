import logo from './logo.svg';
import './App.css';
import Query from './Components/Query';

function App() {
  return (
    <div className="App">
      <Query />
    </div>
  );
}

export default App;