import pandas
import sqlalchemy
from sqlalchemy.orm import scoped_session, sessionmaker
SERVER = 'servdb1.database.windows.net'
DATABASE = 'querygen'
USERNAME = 'server1'
DRIVER = 'ODBC Driver 17 for SQL Server'
PASSWORD = 'VenkatChan2001'
DB_CONNECTION = f'mssql://{USERNAME}:{PASSWORD}@{SERVER}/{DATABASE}?driver={DRIVER}'

engine = sqlalchemy.create_engine(DB_CONNECTION)
Session = scoped_session(sessionmaker(bind=engine))

def execute_sql_query(sql_query):
    session = Session()
    try:
        results = session.execute(sql_query).fetchall()
        formatted_results = []
        for row in results:
            formatted_results.append(dict(row))
        return formatted_results
    finally:
        session.close()